# Digital electronics 2 - ESP32 Exercises

The repository contains ESP32 lab exercises for bachelor course [*Digital Electronics 2*](https://www.vut.cz/en/students/courses/detail/258370) at Brno University of Technology, Czechia. With ESP32 being used as the main programming platform.

## Exercises

1. For ESP-IDF
    1. [Remote control of LED through HTTP server](labs/01-http_led_control_ESP-IDF/)
    2. [Introduction to FreeRTOS using LED blinking task](labs/02-rtos_blink_task_ESP-IDF/)
1. For PlatformIO
    1. [Remote control of LED through HTTP server](labs/01-http_led_control_PlatformIO/)
    2. [Introduction to FreeRTOS using LED blinking task](labs/02-rtos_blink_task_PlatformIO/)

## Components

The following hardware and software components are mainly used in the lab.

* Devices:
  * [ESP32](https://www.espressif.com/en/products/socs/esp32) 32-bit dual-core processor that operates at up to 240 MHz with integrated Wi-Fi and Bluetooth LE: [ESP-IDF Programming Guide ](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html)

* Development tools:
  * [Visual Studio Code](https://code.visualstudio.com/)
  * [PlatformIO](https://platformio.org/)
  * [ESP-IDF](https://github.com/espressif/esp-idf)

* Other tools:
  * [git](https://git-scm.com/)
