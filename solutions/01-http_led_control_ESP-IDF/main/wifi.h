
#ifndef WIFI
#define WIFI

#include <esp_wifi.h>
#include <esp_log.h>
#include <esp_mac.h>
#include "config.h"

void wifi_init_softap(void);

#endif