#ifndef GPIO
#define GPIO

#include <driver/gpio.h>
#include <esp_event.h>
#include "config.h"

void initialize_gpio(void);

#endif