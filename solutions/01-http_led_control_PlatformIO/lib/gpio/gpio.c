#include "gpio.h"

/* GPIO configuration --------------------------------------------------------*/
void initialize_gpio(void)
{
    /* Create GPIO configuration */
    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = (1ULL << CONFIG_GPIO_LED_PIN);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;

    /* Apply the configuration */
    ESP_ERROR_CHECK(gpio_config(&io_conf));
}