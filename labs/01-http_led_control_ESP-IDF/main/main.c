/* Includes ------------------------------------------------------------------*/
#include <nvs_flash.h>

#include "gpio.h"
#include "web_server.h"
#include "wifi.h"

/* Global variables-----------------------------------------------------------*/
httpd_handle_t server_handle = NULL;

/* Main function -------------------------------------------------------------*/
void app_main(void)
{
    /* Initialize NVS flash */
    nvs_flash_init();

    /* Initialize GPIO */
    initialize_gpio();

    /* Initialize network interface */
    esp_event_loop_create_default();

    /* Initialize WiFi */
    wifi_init_softap();
        
    /* Start Web Server */
    server_handle = start_webserver();
}
