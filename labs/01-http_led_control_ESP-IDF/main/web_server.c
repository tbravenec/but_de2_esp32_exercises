#include "web_server.h"

static const char *TAG = "web_server";

/* Hello handler -------------------------------------------------------------*/
static esp_err_t hello_get_handler(httpd_req_t *req)
{
    const char* resp_str = "Hello, World!";
    httpd_resp_send(req, resp_str, strlen(resp_str));
    return ESP_OK;
}

static const httpd_uri_t hello = {
    .uri       = "/hello",
    .method    = HTTP_GET,
    .handler   = hello_get_handler,
    .user_ctx  = NULL
};

/* Turn ON LED ---------------------------------------------------------------*/

// ToDo: Create handler and http uri to turn ON LED

/* Turn OFF LED --------------------------------------------------------------*/

// ToDo: Create handler and http uri to turn OFF LED

/* Web Server initialization -------------------------------------------------*/
httpd_handle_t start_webserver(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.server_port = 80;

    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    /* Start the server and register URI handlers */
    if (httpd_start(&server, &config) == ESP_OK)
    {
        ESP_LOGI(TAG, "HTTP server started");
        httpd_register_uri_handler(server, &hello);

        // ToDo: Register new uri handlers
    }
    else
    {
        ESP_LOGE(TAG, "Failed to start HTTP server");
    }
    
    return server;
}

/* Web Server close ----------------------------------------------------------*/
esp_err_t stop_webserver(httpd_handle_t server)
{
    esp_err_t ret = ESP_FAIL;

    if (server) 
    {
        /* Stop the httpd server */
        httpd_stop(server);
        ESP_LOGI(TAG, "HTTP server stopped");
        ret = ESP_OK;
    }

    return ret;
}