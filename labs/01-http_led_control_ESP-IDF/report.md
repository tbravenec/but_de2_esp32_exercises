# Lab ESP32-1: INSERT_YOUR_FIRSTNAME INSERT_YOUR_LASTNAME

### Control of LED pin through WiFi and HTTP server

1. Fill in the following table and enter the required values for configuration of LED pin using `gpio_config_t` structure using the ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html).

    | **GPIO Config Fields**   | **Value**         | **Description**      |
    |--------------------------|-------------------|----------------------|
    | `intr_type`              | GPIO_INTR_DISABLE | GPIO interrupt type  |
    | `mode`                   |                   |                      |
    | `pin_bit_mask`           |                   |                      |
    | `pull_down_en`           |                   |                      |
    | `pull_up_en`             |                   |                      |

2. Fill in the following table and enter the required values for configuration of WiFi Access Point using `wifi_ap_config_t` structure using the ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html).

    | **WiFI AP Config Fields** | **Value**              | **Description**            |
    |---------------------------|------------------------|----------------------------|
    | `ssid`                    |                        |                            |
    | `password`                |                        |                            |
    | `ssid_len`                |                        |                            |
    | `channel`                 |                        |                            |
    | `authmode`                | WIFI_AUTH_WPA_WPA2_PSK | Auth mode of ESP32 soft-AP |
    | `max_connection`          |                        |                            |