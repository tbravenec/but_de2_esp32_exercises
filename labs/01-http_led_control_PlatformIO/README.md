# Lab 1: Control of LED pin through WiFi and HTTP server

### Learning objectives

After completing this lab you will be able to:

* Configure input/output ports of ESP32 using configuration structures
* Configure and start a WiFi in Access Point mode
* Control ESP32 through web browser
* Use ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html) and find information

The purpose of this laboratory exercise is to learn basics of programming ESP32 microcontroller, how to configure pins, wireless interface and http server. Specifically, it is going to be a control of LED through commands inserted into the web brower URL field..

### Table of contents

* [Pre-Lab preparation](#preparation)
* [Part 1: Synchronize repositories and create a new project](#part1)
* [Part 2: GPIO configuration](#part2)
* [Part 3: WiFI configuration](#part3)
* [Part 4: Using HTTP Server to control LED](#part4)
* [Experiments on your own](#experiments)
* [Post-Lab report](#report)
* [References](#references)

<a name="preparation"></a>

## Pre-Lab preparation

1. Fill in the following table and enter the required values for configuration of LED pin using `gpio_config_t` structure using the ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html).

    | **GPIO Config Fields**   | **Value**         | **Description**      |
    |--------------------------|-------------------|----------------------|
    | `intr_type`              | GPIO_INTR_DISABLE | GPIO interrupt type  |
    | `mode`                   |                   |                      |
    | `pin_bit_mask`           |                   |                      |
    | `pull_down_en`           |                   |                      |
    | `pull_up_en`             |                   |                      |

2. Fill in the following table and enter the required values for configuration of WiFi Access Point using `wifi_ap_config_t` structure using the ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html).

    | **WiFI AP Config Fields** | **Value**              | **Description**            |
    |---------------------------|------------------------|----------------------------|
    | `ssid`                    |                        |                            |
    | `password`                |                        |                            |
    | `ssid_len`                |                        |                            |
    | `channel`                 |                        |                            |
    | `authmode`                | WIFI_AUTH_WPA_WPA2_PSK | Auth mode of ESP32 soft-AP |
    | `max_connection`          |                        |                            |

<a name="part1"></a>

## Part 1: Synchronize repositories and create a new project

When you start working, always synchronize the contents of your working folder and local repository with remote version at GitHub. This way you are sure that you will not lose any of your changes.

1. Run Git Bash (Windows) of Terminal (Linux) in your working directory, and update local repository.

   > **Help:** Useful bash and git commands are `cd` - Change working directory. `mkdir` - Create directory. `ls` - List information about files in the current directory. `pwd` - Print the name of the current working directory. `git status` - Get state of working directory and staging area. `git pull` - Update local repository and working folder.

2. Run Visual Studio Code and create a new PlatformIO project `01-http_led_control_PlatformIO` for `ESP32-CAM` board and change project location to your local repository folder `Documents/digital-electronics-2`

3. Right-click on project name and create a new file `README.md`. Copy/paste [report template](report.md) to your `01-http_led_control_PlatformIO > README.md` file.

<a name="part2"></a>

## Part 2: GPIO configuration

ESP32 microcontroller contains many general purpose pins. Each of the pins is controlled separately and can function as an input (entry) or output (exit) point of the microcontroller. Control is possible exclusively by software via configuration structures.

A description of the GPIO configuration can be found in [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html?highlight=gpio_config_t#gpio-rtc-gpio) in section GPIO & RTC GPIO.

1. Look through the [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html?highlight=gpio_config_t#gpio-rtc-gpio) and find out how to configure the GPIO configuration structure.

2. Check the schematic of the board, and determine the PIN number, need for pull up/down resistors etc.

3. The definition and application of the configuration structure is already prepared in `lib > gpio > gpio.c`. Your task is to fill in all of the fields, to correctly configure the pin with LED. Use the `include > config.h` defines to help with code readability.

4. You can test the configuration by using a function to set pin to either high or low level.

<a name="part3"></a>

## Part 3: WiFI configuration

Now that the GPIO is configured, the next step is the configuration of WiFi. The WiFi interface of ESP32 can work in many modes, be it an Access Point, to which devices can connect to, or a station connecting to an existing WiFi. In this project we are going to configure the WiFi interface as Access Point to which we are going to connect later.

1. Customize the SSID (name of the WiFi), password, channel etc. in `include > config.h`. Select one of the 13 (0-12) european channels available in the 2.4GHz band - Channel 14 is for Japan only.

    > **Important:** Change the channel number. There are only 13 (0-12) channels available and all ESP32s in class transmitting on a single channel would result ina  lot of interference.
    
![WiFi channels](../../images/wifi_channels.png)

2. Now that you have customized your WiFi settings, next step is to create WiFi configuration structure. The goal is to check the WiFi section of the [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_wifi.html?highlight=wifi_config_t#wi-fi) and finish the Access Point configuration of WiFi in `lib > wifi > wifi.c` using defines from `include > config.h`.

3. You can check that the WiFi Access Point is created using your phone. Go to settings and check that you can see your the SSID you have set in previous steps. You can also try to connect to the WiFi. 

    > **Important:** There is a chance the phone will ask you, if you want to stay connected, since there is no internet connection available. Stay connected, as we are going to control the ESP32 through this connection.


<a name="part4"></a>

## Part 4: Using HTTP Server to control LED

The HTTP server is already set up and running on the ESP32. To test it we can connect to the WiFi and go to the address [192.168.4.1/hello](192.168.4.1/hello). In case everything works correctly, the return should be `Hello, World!`. The IP address is the address of the ESP32 on the WiFi network it creates. The part `/hello` in the address is called an Uniform Resource Identifier (URI). The goal of this part, is to create our own URIs to turn the LED ON and to turn it OFF.

1. Study the `lib > web_server > web_server.c` file, in which the HTTP server is defined. There is also defined the URI `/hello` and the function that is run when this address is accessed. The URI also needs to be registered with the HTTP server, which happens just after the server starts in function `start_webserver`.

2. Define your own, custom URIs to control the LED (you can also return message back to the browser, but it is not necessary.)

3. Register your own URIs with the HTTP server and test if the LED can be controlled by accessing appropriate address.

4. When you finish, always synchronize the contents of your working folder with the local and remote versions of your repository. This way you are sure that you will not lose any of your changes. To do that, use **Source Control (Ctrl+Shift+G)** in Visual Studio Code or git commands.

   > **Help:** Useful git commands are `git status` - Get state of working directory and staging area. `git add` - Add new and modified files to the staging area. `git commit` - Record changes to the local repository. `git push` - Push changes to remote repository. `git pull` - Update local repository and working folder. Note that, a brief description of useful git commands can be found [here](https://github.com/tomas-fryza/digital-electronics-1/wiki/Useful-Git-commands) and detailed description of all commands is [here](https://github.com/joshnh/Git-Commands).

<a name="experiments"></a>

## Experiments on your own

1. Respond to accessing the address not just with a simple text message, but return a simple HTML page.

3. Connect another device through the GPIO pins, and access the information from the device using HTTP server.

<a name="report"></a>

## Post-Lab report

*Complete all parts of `01-http_led_control_PlatformIO > README.md` file (see Part 1.3) in Czech, Slovak, or English, push it to your GitHub repository, and submit a link to this file via [BUT e-learning](https://moodle.vutbr.cz/). The deadline for submitting the task is the day before the next lab, i.e. in one week.*

*Vypracujte všechny části ze souboru `01-http_led_control_PlatformIO > README.md` (viz Část 1.3) v českém, slovenském, nebo anglickém jazyce, uložte je na váš GitHub repozitář a odevzdejte link na tento soubor prostřednictvím [e-learningu VUT](https://moodle.vutbr.cz/). Termín odevzdání úkolu je den před dalším laboratorním cvičením, tj. za jeden týden.*

<a name="references"></a>

## References

1. [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html)

2. [ESP32 Tutorials](https://esp32tutorials.com/category/esp32-esp-idf-tutorials/)

3. Tomas Fryza. [Useful Git commands](https://github.com/tomas-fryza/digital-electronics-2/wiki/Useful-Git-commands)

4. [Goxygen commands](http://www.doxygen.nl/manual/docblocks.html#specialblock)
