#ifndef WEB_SERVER
#define WEB_SERVER

#include <esp_log.h>
#include <esp_http_server.h>
#include <driver/gpio.h>

#include "../../include/config.h"

httpd_handle_t start_webserver(void);
esp_err_t stop_webserver(httpd_handle_t server);

#endif