#include "gpio.h"

/* GPIO configuration --------------------------------------------------------*/
void initialize_gpio(void)
{
    /* Create GPIO configuration */
    gpio_config_t io_conf = {};
    
    // ToDo: Configure PIN with LED for output

    /* Apply the configuration */
    ESP_ERROR_CHECK(gpio_config(&io_conf));
}