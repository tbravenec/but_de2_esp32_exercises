# Lab 2: Blinking LED through RTOS tasks and ISRs

### Learning objectives

After completing this lab you will be able to:

* Configure input/output ports of ESP32 using configuration structures
* Create Real Time Operating System (RTOS) tasks
* Use GPIO Interrupt Service Routines (ISR)

The purpose of this laboratory exercise is to continue learning basics of programming with ESP32 microcontroller, how to configure input pins with ISR and create RTOS tasks. Specifically, it is going to be a control of LED blinking through RTOS task, with option to enable or diable blinking on press of a button. Furthermore, the control through HTTP server introduced in previous exercise is still available.

### Table of contents

* [Pre-Lab preparation](#preparation)
* [Part 1: Synchronize repositories and create a new project](#part1)
* [Part 2: Template update](#part2)
* [Part 3: Set up the Blinking task](#part3)
* [Part 4: GPIO configuration with ISR settings](#part4)
* [Experiments on your own](#experiments)
* [Post-Lab report](#report)
* [References](#references)

<a name="preparation"></a>

## Pre-Lab preparation

1. Fill in the following table and enter the required values for configuration of button pin using `gpio_config_t` structure using the ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html), configure the pin as input and to react with an interrupt on the rising edge.

    | **GPIO Config Fields**   | **Value**         | **Description**      |
    |--------------------------|-------------------|----------------------|
    | `intr_type`              |                   | GPIO interrupt type  |
    | `mode`                   |                   |                      |
    | `pin_bit_mask`           |                   |                      |
    | `pull_down_en`           |                   |                      |
    | `pull_up_en`             |                   |                      |

<a name="part1"></a>

## Part 1: Synchronize repositories and create a new project

When you start working, always synchronize the contents of your working folder and local repository with remote version at GitHub. This way you are sure that you will not lose any of your changes.

1. Run Git Bash (Windows) of Terminal (Linux) in your working directory, and update local repository.

   > **Help:** Useful bash and git commands are `cd` - Change working directory. `mkdir` - Create directory. `ls` - List information about files in the current directory. `pwd` - Print the name of the current working directory. `git status` - Get state of working directory and staging area. `git pull` - Update local repository and working folder.

2.  Copy the template of the project `02-rtos_blink_task_ESP-IDF` to your local repository folder `Documents/digital-electronics-2`. Run Visual Studio Code and open the `02-rtos_blink_task_ESP-IDF`. 

3. IMPORTANT: In the bottom left corner select proper port and board `esp32`.

4. Right-click on project name and create a new file `README.md`. Copy/paste [report template](report.md) to your `02-rtos_blink_task_ESP-IDF > README.md` file.

<a name="part2"></a>

## Part 2: Template update

This exercise is based on the previous exercise based on ESP32. As such we are going to reuse the GPIO and WiFi configuration done in the previous exercise. You can just copy these from previous exercise.

1. Update the `main > config.h` defines with your own SSID, Password, WiFi channel. 

2. Update the WiFi configuration in `main > wifi.c`.

3. Update the LED GPIO configuration in `main > gpio.c`.

At this point, the program should work exactly the same as in previous exercise. You can test this, by connecting to the WiFi access point created by the ESP32 and using [192.168.4.1/on](192.168.4.1/on) or [192.168.4.1/off](192.168.4.1/off) to see the LED control.

<a name="part3"></a>

## Part 3: Set up the Blinking task

Tasks in RTOS can be thought of as infinite loops, but there can be many of them. Using non-blocking delays or waiting functions that define at what time the code should continue, the RTOS can switch between tasks and work on several tasks at once.

The LED PIN configuration is simple, you can reuse the configuration from previous exercise. The difference from previous exercise is, that we now use a variable `led_state` to keep track if the LED is turned ON or OFF. The variable is defined in `main > main.c`, and to have access to it from other files, the prefix extern is used in files we need an access to it (`main > gpio.c` and `main > web_server.c`).

1. The prototype and the definition of the `blink_task` function are already ready in the `main > main.c` file.

2. Study the code in `main > main.c` and understand how to create simple tasks. 
    > **Help:** Pay attention to the `xTaskCreate` function call. 

3. Complete the `blink_task` function, to toggle the state of the LED. Use the `led_state` variable to keep track of the current state of the LED.
    > **Idea:** You can change the blinking frequency using `CONFIG_BLINK_FREQUENCY_MS` in `main > config.h`.

<a name="part4"></a>

## Part 4: GPIO configuration with ISR settings

ESP32 microcontroller contains many general purpose pins. Each of the pins is controlled separately and can function as an input (entry) or output (exit) point of the microcontroller. Control is possible exclusively by software via configuration structures.

With ESP32 microcontroller, there are several steps before we can start using ISRs. First is the GPIO PIN configuration, This we follow by installing the ISR service using function `gpio_install_isr_service` and then we assign the ISR handler using `gpio_isr_handler_add` function.

To not overwhelm the microcontroller, it is a good practice to have the code in the ISR handler as computationally unintensive as possible. Since we are using RTOS, we are going to employ Queues and Tasks to handle our ISR from a button press.

At first, we need to create a queue using function `xQueueCreate`. In our case, the queue for handling GPIO ISRs is called `gpio_evt_queue`.

The ISR Handler does only a single command, and that is sending the pin, on which the ISR originated through the  Queue (`gpio_evt_queue`). The information about the ISR is then read in the `gpio_task`.

1. Look through the [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/gpio.html?highlight=gpio_config_t#gpio-rtc-gpio) and find out how to configure the GPIO configuration structure.

2. Check the schematic of the board, and determine the PIN number, need for pull up/down resistors etc.

3. The definition and application of the configuration structure is already prepared in `main > gpio.c`. Your task is to fill in all of the fields, to correctly configure the pin with BUTTON as input and with interrupt settings. Use the `main > config.h` defines to help with code readability.

    > **Idea:** You can test the configuration by using a function in the ISR handler (`gpio_isr_handler`) to turn LED ON or OFF.

4. Complete the `gpio_task` function to toggle the blinking task. Use the variable `blink_enable` to do this. Test that the button stops and starts blinking.

5. When you finish, always synchronize the contents of your working folder with the local and remote versions of your repository. This way you are sure that you will not lose any of your changes. To do that, use **Source Control (Ctrl+Shift+G)** in Visual Studio Code or git commands.

   > **Help:** Useful git commands are `git status` - Get state of working directory and staging area. `git add` - Add new and modified files to the staging area. `git commit` - Record changes to the local repository. `git push` - Push changes to remote repository. `git pull` - Update local repository and working folder. Note that, a brief description of useful git commands can be found [here](https://github.com/tomas-fryza/digital-electronics-1/wiki/Useful-Git-commands) and detailed description of all commands is [here](https://github.com/joshnh/Git-Commands).

<a name="experiments"></a>

## Experiments on your own

1. Implement enable/disable the button through the HTTP server commands.

3. Implement blinking enable/disable through the http server.

<a name="report"></a>

## Post-Lab report

*Complete all parts of `02-rtos_blink_task_ESP-IDF > README.md` file (see Part 1.4) in Czech, Slovak, or English, push it to your GitHub repository, and submit a link to this file via [BUT e-learning](https://moodle.vutbr.cz/). The deadline for submitting the task is the day before the next lab, i.e. in one week.*

*Vypracujte všechny části ze souboru `02-rtos_blink_task_ESP-IDF > README.md` (viz Část 1.4) v českém, slovenském, nebo anglickém jazyce, uložte je na váš GitHub repozitář a odevzdejte link na tento soubor prostřednictvím [e-learningu VUT](https://moodle.vutbr.cz/). Termín odevzdání úkolu je den před dalším laboratorním cvičením, tj. za jeden týden.*

<a name="references"></a>

## References

1. [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html)

2. [ESP32 Tutorials](https://esp32tutorials.com/category/esp32-esp-idf-tutorials/)

3. [FreeRTOS](https://www.freertos.org)

3. Tomas Fryza. [Useful Git commands](https://github.com/tomas-fryza/digital-electronics-2/wiki/Useful-Git-commands)

4. [Goxygen commands](http://www.doxygen.nl/manual/docblocks.html#specialblock)
