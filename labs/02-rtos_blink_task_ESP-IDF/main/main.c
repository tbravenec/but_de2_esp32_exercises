/* Includes ------------------------------------------------------------------*/
#include <nvs_flash.h>

#include "gpio.h"
#include "web_server.h"
#include "wifi.h"

/* Global variables-----------------------------------------------------------*/
httpd_handle_t server_handle = NULL;

volatile bool blink_enable = true;
volatile int led_state = CONFIG_GPIO_LED_OFF;

/* Task prototypes -----------------------------------------------------------*/
static void blink_task(void* arg);

/* Main function -------------------------------------------------------------*/
void app_main(void)
{
    /* Initialize NVS flash */
    nvs_flash_init();

    /* Initialize GPIO */
    initialize_gpio();

    /* Initialize network interface */
    esp_event_loop_create_default();

    /* Initialize WiFi */
    wifi_init_softap();
        
    /* Start Web Server */
    server_handle = start_webserver();

    /* Create Blink Task */
    xTaskCreate(blink_task, "blink_task", 1024, NULL, 10, NULL);
}

/* Task definitions ----------------------------------------------------------*/
static void blink_task(void* arg)
{
    /* Calculate delay constant */
    const TickType_t xDelay = CONFIG_BLINK_FREQUENCY_MS / portTICK_PERIOD_MS;

    while(true)
    {
        /* Check if blinking is enabled */
        if (blink_enable)
        {
            // ToDo: Write a code to Toggle LED, use led_state variable         
        }
        vTaskDelay(xDelay);
    }
}