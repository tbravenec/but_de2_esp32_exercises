#include "gpio.h"

/* Defines -------------------------------------------------------------------*/
#define ESP_INTR_FLAG_DEFAULT 0

/* Global variables-----------------------------------------------------------*/
extern bool blink_enable;
extern int led_state;

static QueueHandle_t gpio_evt_queue = NULL;

/* Interrupt service prototypes ----------------------------------------------*/
static void IRAM_ATTR gpio_isr_handler(void* arg);

/* Task prototypes -----------------------------------------------------------*/
static void gpio_task(void* arg);

/* GPIO configuration --------------------------------------------------------*/
void initialize_gpio(void)
{
    /* Create LED PIN configuration */
    gpio_config_t io_conf = {};
    
    // ToDo: Configure PIN with LED for output (copy from previous exercise)

    /* Apply the configuration */
    ESP_ERROR_CHECK(gpio_config(&io_conf));

    /* Apply the configuration */
    ESP_ERROR_CHECK(gpio_set_level(CONFIG_GPIO_LED_PIN, CONFIG_GPIO_LED_ON));

    /* Store current LED state */
    led_state = CONFIG_GPIO_LED_ON;
    
    /* Create Button PIN configuration */
    
    // ToDo: Configure PIN with button as input
    
    /* Apply the configuration */
    ESP_ERROR_CHECK(gpio_config(&io_conf));

    /* Create a queue to handle GPIO event from ISR */
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

    /* Start GPIO task */
    xTaskCreate(gpio_task, "gpio_task", 2048, NULL, 10, NULL);

    /* Install GPIO ISR service */
    ESP_ERROR_CHECK(gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT));

    /* Connect Button PIN to ISR handler */
    ESP_ERROR_CHECK(gpio_isr_handler_add(CONFIG_GPIO_BUTTON_PIN, gpio_isr_handler, (void*) CONFIG_GPIO_BUTTON_PIN));
}

/* Interrupt service routines ------------------------------------------------*/
static void gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;

    /* Update queue from ISR */
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

/* Task definitions ----------------------------------------------------------*/
static void gpio_task(void* arg)
{
    uint32_t io_num;

    while(true)
    {
        /* Receive info about ISR from queue */
        if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            // ToDo: Update the blink_enable variable
        }
    }
}