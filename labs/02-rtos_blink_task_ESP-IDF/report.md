# Lab ESP32-2: INSERT_YOUR_FIRSTNAME INSERT_YOUR_LASTNAME

### Blinking LED through RTOS tasks and ISRs

1. Fill in the following table and enter the required values for configuration of button pin using `gpio_config_t` structure using the ESP32 [ESP-IDF Programming Guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html), configure the pin as input and to react with an interrupt on the rising edge.

    | **GPIO Config Fields**   | **Value**         | **Description**      |
    |--------------------------|-------------------|----------------------|
    | `intr_type`              |                   | GPIO interrupt type  |
    | `mode`                   |                   |                      |
    | `pin_bit_mask`           |                   |                      |
    | `pull_down_en`           |                   |                      |
    | `pull_up_en`             |                   |                      |
